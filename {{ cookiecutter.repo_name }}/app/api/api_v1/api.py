from fastapi import APIRouter
from app.api.api_v1.endpoints import predict, feedback

api_router = APIRouter()
api_router.include_router(predict.router, prefix="/predict")