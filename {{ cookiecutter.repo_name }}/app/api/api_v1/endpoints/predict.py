from typing import Any
from app import schemas
from fastapi import BackgroundTasks, APIRouter
from onnxruntime import InferenceSession
from whylogs.viz.utils.drift_calculations import calculate_drift_values
import numpy as np
import pandas as pd
from prometheus_client import Gauge
import whylogs as why
from app.core.config import settings

DRIFT_VALUES = Gauge('drift_values', 'p-value for applied statistical test', ['column', 'test'])
session = InferenceSession(settings.MODEL_PATH)
reference = why.read(settings.PROFILE_PATH)
target = None
router = APIRouter()

def export_metrics(dataframe: pd.DataFrame):
    # Calculate drift values
    global target
    if target is None:
        results = why.log(pandas=dataframe)
        target = results.profile()  
    else:
        target.track(pandas=dataframe)
    drift_values = calculate_drift_values(target.view(), reference.view())
    
    # Update drift values
    for key, value in drift_values.items():
        DRIFT_VALUES.labels(key, value["test"]).set(value["p_value"])

@router.post("/", response_model=schemas.Prediction)
def predict(
    *,
    background_tasks: BackgroundTasks,
    payload: schemas.Payload
) -> Any:  
    # Process payload data
    payload = [payload.dict()]
    payload = pd.DataFrame(payload)

    # Export data drift values
    background_tasks.add_task(export_metrics, payload)

    # Predict output
    input_value = payload.to_numpy().astype(np.float32)
    input_name = session.get_inputs()[0].name
    output_value = session.run(None, {input_name: input_value})[0]

    # Generate response
    prediction = schemas.Prediction(value=output_value[0][0])
    return prediction