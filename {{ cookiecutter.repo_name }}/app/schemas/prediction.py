from pydantic import BaseModel, Field

class Prediction(BaseModel):
    value: float = Field(example=0.0)