from pydantic import BaseModel, Field

class Payload(BaseModel):
    feature_0: float = Field(example=0.0)
    feature_1: float = Field(example=0.0)
    feature_2: float = Field(example=0.0)
    feature_3: float = Field(example=0.0)
    feature_4: float = Field(example=0.0)