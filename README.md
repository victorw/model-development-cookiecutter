# Model Development Cookiecutter

This is a [cookiecutter](https://github.com/audreyr/cookiecutter) template for data science projects. (Supports Python ≥ 3.9)
