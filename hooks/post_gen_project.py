import logging
import sys
import shutil
import os 

logging.basicConfig()
_logger = logging.getLogger(__name__)

def clean_files():
    use_gitlab_ci = "{{cookiecutter.use_gitlab_ci}}"
    use_model_service = "{{cookiecutter.use_model_service}}"
    use_code_template = "{{cookiecutter.use_code_template}}"
    to_delete = []

    if use_gitlab_ci == "False":
        to_delete = to_delete + [".gitlab-ci.yml"]

    if use_model_service == "False":
        to_delete = to_delete + ["app", "Dockerfile", ".env.example"]

    if use_code_template == "False":
        to_delete = to_delete + ["src/data", "src/model"]

    to_delete = to_delete + ["data/.gitkeep", "models/.gitkeep", "reports/.gitkeep"]

    try:
        for file_or_dir in to_delete:
            if os.path.isfile(file_or_dir):
                os.remove(file_or_dir)
            else:
                shutil.rmtree(file_or_dir)
    except OSError as e:
        _logger.warning("While attempting to remove file(s) an error occurred")
        _logger.warning(f"Error: {e}")
        sys.exit(1)

if __name__ == "__main__":
    clean_files()